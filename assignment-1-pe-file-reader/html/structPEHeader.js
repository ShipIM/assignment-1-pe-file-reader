var structPEHeader =
[
    [ "characteristics", "structPEHeader.html#a6827d3143635547c721a135da9dd7bbb", null ],
    [ "machine", "structPEHeader.html#a76ce07d9deafcca4f6e5358a52e826ba", null ],
    [ "magic", "structPEHeader.html#ad1dfae775f8b7007b6dd983796791359", null ],
    [ "number_of_sections", "structPEHeader.html#a94f39193dbace9f42001eb1abb0d9a22", null ],
    [ "number_of_symbols", "structPEHeader.html#a88f7cc3e194a4673b9ef854eb8e50c37", null ],
    [ "pointer_to_symbol_table", "structPEHeader.html#a5ac34b2fe9f08a00cb978121530456a6", null ],
    [ "size_of_optional_header", "structPEHeader.html#a41b5cc73d344ba92afcebe1f3a7312b3", null ],
    [ "time_date_stamp", "structPEHeader.html#aa5590e156dbb39b8a19a9fad662b4e0b", null ]
];