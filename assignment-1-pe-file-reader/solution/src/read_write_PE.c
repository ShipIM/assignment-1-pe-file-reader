#include "../include/read_write_PE.h"
#include <stdlib.h>
#include <string.h>

static enum read_status read_PE_header(FILE *in, struct PEHeader *header)
{
    if (fseek(in, PE_HEADER_OFFSET, SEEK_SET))
        return READ_UNABLE_TO_READ;

    uint32_t offset;
    if (fread(&offset, sizeof(int32_t), 1, in) != 1)
        return READ_UNABLE_TO_READ;

    if (fseek(in, offset, SEEK_SET))
        return READ_UNABLE_TO_READ;

    if (fread(header, sizeof(struct PEHeader), 1, in) != 1)
        return READ_UNABLE_TO_READ;

    return READ_OK;
}

static enum read_status read_section_header(FILE *in, struct SectionHeader *header)
{
    if (fread(header, sizeof(struct SectionHeader), 1, in) != 1)
        return READ_UNABLE_TO_READ;

    return READ_OK;
}

enum read_status find_section_header_by_name(FILE *in, char *name, struct SectionHeader *section_header)
{
    struct PEHeader PE_header;
    if (read_PE_header(in, &PE_header) != READ_OK)
        return READ_UNABLE_TO_READ;

    if(fseek(in, PE_header.size_of_optional_header, SEEK_CUR))
        return READ_UNABLE_TO_READ;

    for (size_t i = 0; i < PE_header.number_of_sections; i++)
    {
        if(read_section_header(in, section_header))
            return READ_UNABLE_TO_READ;

        if (strcmp(section_header->name, name) == 0)
            return READ_OK;
    }

    return READ_UNABLE_TO_READ;
}

enum write_status print_section_contents(FILE *in, FILE *out, struct SectionHeader *section_header)
{
    if (fseek(in, section_header->pointer_to_raw_data, SEEK_SET))
        return WRITE_SOURCE_ERROR;

    uint8_t content;
    for (int i = 0; i < section_header->size_of_raw_data; i += sizeof(uint8_t))
    {
        if (fread(&content, sizeof(uint8_t), 1, in) == 1)
            fwrite(&content, sizeof(uint8_t), 1, out);
        else
            return WRITE_ERROR;
    }

    return WRITE_OK;
}
