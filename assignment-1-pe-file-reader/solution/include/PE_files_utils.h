#ifndef PE_files_itils_h
#define PE_files_itils_h

#include <inttypes.h>
#include <stdbool.h>

#ifdef _MSC_VER
typedef unsigned __int16 uint16_t typedef unsigned __int32 uint32_t
#else
#include <stdint.h>
#endif

#define SIZE_OF_NAME 8
#define PE_HEADER_OFFSET 0x3c

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
    struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
    PEHeader
{
    uint32_t magic;
    uint16_t machine;
    uint16_t number_of_sections;
    uint32_t time_date_stamp;
    uint32_t pointer_to_symbol_table;
    uint32_t number_of_symbols;
    uint16_t size_of_optional_header;
    uint16_t characteristics;
};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
    SectionHeader
{
    char name[SIZE_OF_NAME];
    uint32_t virtual_size;
    uint32_t virtual_address;
    uint32_t size_of_raw_data;
    uint32_t pointer_to_raw_data;
    uint32_t pointer_to_relocations;
    uint32_t pointer_to_linenumbers;
    uint16_t number_of_relocations;
    uint16_t number_of_linenumbers;
    uint32_t characteristics;
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif

#endif
