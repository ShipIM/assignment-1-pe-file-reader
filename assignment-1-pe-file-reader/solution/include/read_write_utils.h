#ifndef read_write_utils_h
#define read_write_utils_h

enum read_status
{
  READ_OK = 0,
  READ_UNABLE_TO_READ
};

enum write_status
{
  WRITE_OK = 0,
  WRITE_SOURCE_ERROR,
  WRITE_ERROR
};

#endif
