#ifndef read_write_PE_h
#define read_write_PE_h

#include "PE_files_utils.h"
#include "read_write_utils.h"
#include <stdio.h>

enum read_status find_section_header_by_name(FILE *in, char *name, struct SectionHeader *section_header);
enum write_status print_section_contents(FILE *in, FILE *out, struct SectionHeader *section_header);

#endif
