/// @file
/// @brief Header file contains various enumerations as a result of the execution of functions

#ifndef read_write_utils_h
#define read_write_utils_h

/// enum for read functions
enum read_status
{
  READ_OK = 0 /** in case of success */, 
  READ_UNABLE_TO_READ /** in case of common error connected to read operations */
};

/// enum for write functions
enum write_status
{
  WRITE_OK = 0 /** in case of success */,
  WRITE_SOURCE_ERROR /** in case of an error caused by problems with the source code during write operations */, 
  WRITE_ERROR /** in case of an common write operations errors */
};

#endif
