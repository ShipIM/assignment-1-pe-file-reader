/// @file
/// @brief Header file contains a description of the functions for interacting with files

#ifndef open_close_files_h
#define open_close_files_h

#include <stdbool.h>
#include <stdio.h>

/// @brief function for opening files
/// @param[in] file reference to interact with
/// @param[in] filename Name of the file to be opened
/// @param[in] mode Options for file opening
/// @return true in case of success or false
bool open_file(FILE **file, char *filename, char *mode);

/// @brief function for closing files
/// @param[in] file reference to interact with
/// @return true in case of success or false
bool close_file(FILE **file);

#endif
