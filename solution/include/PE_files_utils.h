/// @file
/// @brief Header file contains structures for PE files

#ifndef PE_files_itils_h
#define PE_files_itils_h

#include <inttypes.h>
#include <stdbool.h>

#ifdef _MSC_VER
typedef unsigned __int16 uint16_t typedef unsigned __int32 uint32_t
#else
#include <stdint.h>
#endif

/// Size of name of the section
#define SIZE_OF_NAME 8

/// Main offset that leads to PE file header
#define PE_HEADER_OFFSET 0x3c

/**
 * @brief Structure containing the PE header data
 * The PE header contains information about the executable file,
 * such as the machine type, number of sections,
 * creation time stamp, symbol table information, and file attributes
 */
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
    struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
    PEHeader
{
    /// Magic number, indicates the file type
    /// @note In general, it is not part of the header, but this field has been included for ease of use.
    uint32_t magic;
    /// The number that identifies the type of target machine.
    uint16_t machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers. 
    uint16_t number_of_sections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created. 
    uint32_t time_date_stamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present.
    uint32_t pointer_to_symbol_table;
    /// The number of entries in the symbol table.
    uint32_t number_of_symbols;
    /// The size of the optional header, which is required for executable files but not for object files.
    uint16_t size_of_optional_header;
    /// The flags that indicate the attributes of the file.
    uint16_t characteristics;
};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/**
 * @brief The SectionHeader struct represents a section header in a PE file
 * The section header contains information about a specific section in the PE file,
 * such as its name, virtual and physical address, size, etc
 */
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
    SectionHeader
{
    /// An 8-byte, null-padded UTF-8 encoded string.
    char name[SIZE_OF_NAME];
    /// The total size of the section when loaded into memory.
    uint32_t virtual_size;
    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory. 
    uint32_t virtual_address;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files).
    uint32_t size_of_raw_data;
    /// The file pointer to the first page of the section within the COFF file.
    uint32_t pointer_to_raw_data;
    /// The file pointer to the beginning of relocation entries for the section.
    uint32_t pointer_to_relocations;
    /// The file pointer to the beginning of line-number entries for the section.
    uint32_t pointer_to_linenumbers;
    /// The number of relocation entries for the section.
    uint16_t number_of_relocations;
    /// The number of line-number entries for the section.
    uint16_t number_of_linenumbers;
    /// The flags that describe the characteristics of the section.
    uint32_t characteristics;
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif

#endif
