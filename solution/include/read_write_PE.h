/// @file
/// @brief  Header file contains a description of the functions for interacting with PE files

#ifndef read_write_PE_h
#define read_write_PE_h

#include "PE_files_utils.h"
#include "read_write_utils.h"
#include <stdio.h>

/// @brief function for finding section header in PE file by its name
/// @param[in] in Reference to the file source
/// @param[in] name Name of the section to be found
/// @param[in] section_header Reference to interact with
/// @return one of the read_status enum conditions
enum read_status find_section_header_by_name(FILE *in, char *name, struct SectionHeader *section_header);

//// @brief function for printing section contents in output file
/// @param[in] in Reference to the file source
/// @param[in] out Reference to the file output
/// @param[in] section_header Reference to interact with
/// @return one of the write_status enum conditions
enum write_status print_section_contents(FILE *in, FILE *out, struct SectionHeader *section_header);

#endif
