/// @file
/// @brief Main application file

#include "../include/read_write_PE.h"
#include "../include/open_close_files.h"
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char **argv)
{
  usage(stdout);

  if (argc != 4)
  {
    fprintf(stderr, "Something is wrong with the entered arguments");

    return 1;
  }

  FILE *input;
  FILE *output;

  if (open_file(&input, argv[1], "rb+") != 1)
  {
    fprintf(stderr, "Can't open read file");

    return 1;
  }

  if (open_file(&output, argv[3], "wb+") != 1)
  {
    fprintf(stderr, "Can't open write file");

    if (close_file(&input) != 1)
      fprintf(stderr, "Can't close read file");

    return 1;
  }

  struct SectionHeader section_header;

  if (find_section_header_by_name(input, argv[2], &section_header) != READ_OK)
    fprintf(stderr, "Unable to find section header with such name.");
  else if (print_section_contents(input, output, &section_header))
    fprintf(stderr, "Unable to print section.");

  if (close_file(&input) != 1)
  {
    fprintf(stderr, "Can't close write file");

    if (close_file(&output) != 1)
      fprintf(stderr, "Can't close write file");

    return 1;
  }

  if (close_file(&output) != 1)
  {
    fprintf(stderr, "Can't close write file");

    return 1;
  }

  return 0;
}
