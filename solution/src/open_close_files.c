#include "open_close_files.h"

/// @brief function for opening files
/// @param[in] file reference to interact with
/// @param[in] filename Name of the file to be opened
/// @param[in] mode Options for file opening
/// @return true in case of success or false
bool open_file(FILE **file, char *filename, char *mode)
{
    *file = fopen(filename, mode);

    return *file != NULL;
}

/// @brief function for closing files
/// @param[in] file reference to interact with
/// @return true in case of success or false
bool close_file(FILE **file)
{
    return fclose(*file) == 0;
}
