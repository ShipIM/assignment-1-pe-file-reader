var searchData=
[
  ['pe_5ffiles_5futils_2eh_15',['PE_files_utils.h',['../PE__files__utils_8h.html',1,'']]],
  ['pe_5fheader_5foffset_16',['PE_HEADER_OFFSET',['../PE__files__utils_8h.html#a15fd9d1e5df0c57eb45d80413709597a',1,'PE_files_utils.h']]],
  ['peheader_17',['PEHeader',['../structPEHeader.html',1,'']]],
  ['pointer_5fto_5flinenumbers_18',['pointer_to_linenumbers',['../structSectionHeader.html#a76d3e60e9c3a7a188ce96557afb77483',1,'SectionHeader']]],
  ['pointer_5fto_5fraw_5fdata_19',['pointer_to_raw_data',['../structSectionHeader.html#a5827e5ec05f18463d67f9d87039a92d3',1,'SectionHeader']]],
  ['pointer_5fto_5frelocations_20',['pointer_to_relocations',['../structSectionHeader.html#ad637930a56c9b6d609d807379cf36799',1,'SectionHeader']]],
  ['pointer_5fto_5fsymbol_5ftable_21',['pointer_to_symbol_table',['../structPEHeader.html#a5ac34b2fe9f08a00cb978121530456a6',1,'PEHeader']]],
  ['print_5fsection_5fcontents_22',['print_section_contents',['../read__write__PE_8h.html#a67df32dfe86f4e0eb6bb9ad54723dc82',1,'read_write_PE.c']]]
];
