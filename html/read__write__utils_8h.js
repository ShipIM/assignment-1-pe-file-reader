var read__write__utils_8h =
[
    [ "read_status", "read__write__utils_8h.html#a4d84c66cd3c22eb9c38633d500a86303", [
      [ "READ_OK", "read__write__utils_8h.html#a4d84c66cd3c22eb9c38633d500a86303a0967ee309ffe95c5ee502fb9c273e30c", null ],
      [ "READ_UNABLE_TO_READ", "read__write__utils_8h.html#a4d84c66cd3c22eb9c38633d500a86303acc846d12058c399eae49e723cd331d9c", null ]
    ] ],
    [ "write_status", "read__write__utils_8h.html#ae852c29c3f55482704e8b5d38cb07351", [
      [ "WRITE_OK", "read__write__utils_8h.html#ae852c29c3f55482704e8b5d38cb07351a2f895e17d9697fb39aece99ec0e9d496", null ],
      [ "WRITE_SOURCE_ERROR", "read__write__utils_8h.html#ae852c29c3f55482704e8b5d38cb07351a1b37c9326bb29764f772232e0d6348c2", null ],
      [ "WRITE_ERROR", "read__write__utils_8h.html#ae852c29c3f55482704e8b5d38cb07351aa8f1c48fe061ea1c5c35a074f39259d8", null ]
    ] ]
];